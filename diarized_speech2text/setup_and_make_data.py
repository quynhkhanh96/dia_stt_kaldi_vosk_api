import os 
import librosa 
import sys 
from nonspeech_remove import create_segments_file, pyannote_sad

input_wav_path = sys.argv[1]
os.makedirs('./data', exist_ok=True)
os.makedirs('./data/test', exist_ok=True)

file_id = 'spk001_test'
spk_id = 'spk001'

# segments 
create_segments_file(
    file_id=file_id,
    wav_fpath=input_wav_path,
    segments_fpath='./data/test/segments'
)

# wav.scp
with open('./data/test/wav.scp', 'a') as f:
    f.write(f'{file_id} sox {input_wav_path} -c 1 -t wav -r 16000 -b 16 - |')

with open('./data/test/segments', 'r') as f:
    content = f.readlines()
utt_ids = [l.strip().split(' ')[0] for l in content]

# utt2spk
with open('./data/test/utt2spk', 'a') as f:
    for utt_id in utt_ids:
        f.write(f'{utt_id} {spk_id}\n')

# text 
with open('./data/test/text', 'a') as f:
    for utt_id in utt_ids:
        f.write(f'{utt_id} xin chao\n')
