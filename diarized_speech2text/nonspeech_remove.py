import numpy as np 
import librosa 

import torch
from pyannote.audio.utils.signal import Binarize

def pyannote_sad(wav_fpath, sad):
    
    test_file = {'audio': wav_fpath}
    
    sad_scores = sad(test_file)
    binarize = Binarize(offset=0.9, onset=0.9, log_scale=True, 
                        min_duration_off=0.1, min_duration_on=0.1)

    # speech regions (as `pyannote.core.Timeline` instance)
    speech = binarize.apply(sad_scores, dimension=1)

    voiced_segments = []
    for segment in speech:
        voiced_segments.append([segment.start, segment.end])

    return voiced_segments

SAD = torch.hub.load('pyannote/pyannote-audio', 'sad_ami')
def create_segments_file(file_id, 
                        wav_fpath, 
                        segments_fpath):
        
    voiced_segments = pyannote_sad(wav_fpath, SAD)

    new_utt_ids = []
    with open(segments_fpath, 'a') as f:
        utt_index = 1 
        for voiced_segment in voiced_segments:
            start = voiced_segment[0]
            end = voiced_segment[1]
            new_utt_id = f'{file_id}_{utt_index:04}'
            f.write(f'{new_utt_id} {file_id} {start:.3f} {end:.3f}\n')
            new_utt_ids.append(new_utt_id)
            utt_index += 1 

    return new_utt_ids 



