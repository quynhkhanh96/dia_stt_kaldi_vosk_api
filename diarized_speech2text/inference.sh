. ./cmd.sh
. ./path.sh
set -e
mfccdir=data/mfcc
vaddir=data/mfcc
nnet_dir=exp/xvector_nnet_1a
nj=1
stage=0

# - Prepare Features ----------------------------------------------------------
# Make MFCCs
echo '[INFO] ====== Make MFCCs ======'
if [ $stage -le 0 ]; then
    utils/fix_data_dir.sh data/test  
    steps/make_mfcc.sh --mfcc-config conf/mfcc.conf --nj $nj --cmd "$train_cmd" --write-utt2num-frames true data/test exp/make_mfcc $mfccdir
    utils/fix_data_dir.sh data/test 
fi 

# VAD 
echo '[INFO] ====== VAD ======'
if [ $stage -le 1 ]; then
    sid/compute_vad_decision.sh --nj $nj --cmd "$train_cmd" data/test exp/make_vad $vaddir
    utils/fix_data_dir.sh data/test
fi 

# create segments file 
echo '[INFO] ====== Create segments ======'
if [ $stage -le 2 ]; then
    echo "0.01" > data/test/frame_shift
    diarization/vad_to_segments.sh --nj $nj --cmd "$train_cmd" data/test data/test_segmented 
fi
    
# Prepare features 
echo '[INFO] ====== Prepare features for x-vector extraction ======'
if [ $stage -le 3 ]; then
    local/nnet3/xvector/prepare_feats.sh --nj $nj --cmd "$train_cmd" data/test_segmented data/test_segmented_cmn exp/test_segmented_cmn
    cp data/test_segmented/vad.scp data/test_segmented_cmn/
    if [ -f data/test_segmented/segments ]; then
        cp data/test_segmented/segments data/test_segmented_cmn/
    fi
    utils/fix_data_dir.sh data/test_segmented_cmn
fi

# - Extract x-vectors ----------------------------------------------------------
echo '[INFO] ====== Extract x-vectors ======'
if [ $stage -le 4 ]; then
    diarization/nnet3/xvector/extract_xvectors.sh --cmd "$train_cmd --mem 5G" --nj $nj --window 1.5 --period 0.75 --apply-cmn false --min-segment 0.5 $nnet_dir data/test_segmented_cmn $nnet_dir/xvectors_test_segmented_cmn 
fi 
# - Perform PLDA scoring -------------------------------------------------------
echo '[INFO] ====== Perform PLDA scoring ======'
if [ $stage -le 5 ]; then
    diarization/nnet3/xvector/score_plda.sh --cmd "$train_cmd --mem 4G" --nj $nj $nnet_dir/xvectors_vinbdi2 $nnet_dir/xvectors_test_segmented_cmn $nnet_dir/xvectors_test_segmented_cmn/plda_scores
fi 
# - Cluster speakers -----------------------------------------------------------
echo '[INFO] ====== Cluster speakers ======'
if [ $stage -le 6 ]; then
    for threshold in 0.5; do
        # diarization/cluster.sh --cmd "$train_cmd --mem 4G" --nj $nj --threshold $threshold $nnet_dir/xvectors_test_segmented_cmn/plda_scores $nnet_dir/xvectors_test_segmented_cmn/plda_scores_threshold_${threshold}
        diarization/cluster.sh --cmd "$train_cmd --mem 4G" --nj $nj --threshold $threshold $nnet_dir/xvectors_test_segmented_cmn/plda_scores $nnet_dir/xvectors_test_segmented_cmn/plda_scores_threshold_${threshold}
    done 
fi
