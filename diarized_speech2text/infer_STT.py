import os 
import numpy as np 
import pandas as pd 
import pathlib 
import shutil 

from vosk import Model, KaldiRecognizer 
import librosa
import wave  
import json 

NNET_DIR = 'exp/xvector_nnet_1a'
THRESHOLD = 0.5
TRAINED_STT_DIR = '202005-STT-GeneralDomain_-3k5VinData_2kYoutube_XSAMPAPhones_ConventionalTraining_Bigmodel-Thinh-VoskAPI'

sample_rate = 16000

def relabel_speaker_id(rttm_path):
    with open(rttm_path, 'r') as rttm_reader:
        content = rttm_reader.readlines()
    rttm = [[elt for elt in l.strip().split(' ') if elt != ''] for l in content]

    speaker_labels = [int(l[7]) for l in rttm]
    id_ = 0
    unique_ids = []
    relabels = {}
    for label in speaker_labels:
        if label not in unique_ids:
            relabels[label] = id_ 
            id_ += 1
            unique_ids.append(label)

    diaries = []
    for elts in rttm:
        start = float(elts[3])
        dur = float(elts[4])
        spk_id = relabels[int(elts[7])]
        diaries.append([spk_id, start, dur])

    return diaries 

# = STT model inference (get transcript from wave) =================================
class STTInference:
    '''
    Usage: 
    stt_infer = STTInference(TRAINED_STT_DIR)
    stt_infer.get_wf(test_fpath)
    rs_text = stt_infer.get_text_from_wav(0.4, 10.5)
    '''
    def __init__(self, trained_stt_dir):

        self.model = Model(trained_stt_dir)
        self.rec = KaldiRecognizer(self.model, sample_rate)
    
    def get_wf(self, test_fpath):
        wf = wave.open(test_fpath, 'rb')
        nframes = wf.getnframes()
        self.data = wf.readframes(nframes)
        
    def get_text_from_wav(self, start, end):
        utt_text = []
        
        start = int(2 * start * sample_rate)
        end = int(2 * end * sample_rate)
        chunk = 4000
        for i in range(start, end, chunk):
            this_end = min(i + chunk, end)
            seg = self.data[i: this_end]
            if len(seg) == 0:
                break
            if self.rec.AcceptWaveform(seg):
                result = json.loads(self.rec.Result())
                utt_text.append(result['text']) 

        last_result = json.loads(self.rec.FinalResult())
        utt_text.append(last_result['text'])  

        final_result = '. '.join([utt[0].upper() + utt[1: ] for utt in utt_text]) + '.'

        return final_result 

# = Diarize speech ===================================================
class DiarizedSTT:
    def __init__(self, trained_stt_dir=TRAINED_STT_DIR):

        self.trained_stt_dir = trained_stt_dir

    def diarize(self, test_fpath):

        os.chdir('./diarized_speech2text')
        # print('now we are at: ', os.getcwd())
        # create database for test file 
        os.system(f'python setup_and_make_data.py {test_fpath}')
        # perform inference 
        os.system(f'./inference.sh')        
        # relabel rttm file 
        diaries = relabel_speaker_id(f'{NNET_DIR}/xvectors_test_segmented_cmn/plda_scores_threshold_{THRESHOLD}/rttm')

        # remove temp files 
        os.system(f'rm -r {NNET_DIR}/xvectors_test_segmented_cmn')
        os.system(f'rm -r exp/make_mfcc')
        os.system(f'rm -r exp/make_vad')
        os.system(f'rm -r test_segmented_cmn')

        for subdir in os.listdir('data'):
            os.system(f'rm -r data/{subdir}')
         
        speaker_ids = sorted(list(set([x[0] for x in diaries])))
        
        # get format {'speaker_0': [[0.5, 2], [7, 3.4], ...]}
        speaker_diaries = {}
        for speaker_id in speaker_ids:
            this_spk_dia = [[diary[1], diary[2]] for diary in diaries if diary[0] == speaker_id]
            this_spk_dia = sorted(this_spk_dia, key=lambda x: x[0])
            speaker_diaries[speaker_id] = this_spk_dia 
     
        # get transcript for each diarized segment
        diarized_transcripts = {f'speaker_{speaker_id}': [] for speaker_id in speaker_diaries.keys()}
        # diarized_transcripts = {
        #     'speaker_0': [
        #         {
        #             'start': 0.2,
        #             'end': 3.5,
        #             'text': 'xin chao'
        #         },
        #         {
        #             'start': 5.1,
        #             'end': 7.3,
        #             'text': 'ten toi la KK'
        #         }
        #     ],
        #     'speaker_1': [
        #         {
        #             'start': 3.8,
        #             'end': 4.5,
        #             'text': 'chao ban'
        #         },
        #         {
        #             'start': 8.0,
        #             'end': 9.4,
        #             'text': 'rat vui gap ban'
        #         }                
        #     ]
        # }
        for speaker_id in sorted(speaker_diaries, key=lambda x: int(x)):

            for start, dur in speaker_diaries[speaker_id]:
                stt_infer = STTInference(self.trained_stt_dir)
                stt_infer.get_wf(test_fpath)

                end = round(start + dur, 3) 

                text_rs = stt_infer.get_text_from_wav(start, end)
                
                diarized_transcripts[f'speaker_{speaker_id}'] += [{
                    'start': start,
                    'end': end,
                    'text': text_rs
                }, ]

        os.chdir('..')
        # print('now we are at: ', os.getcwd())
        return diarized_transcripts 


if __name__ == '__main__':
 
    test_fpath = '/mnt/4T/a/speaker_diarization/evaluation_data/7to10/diff_gender/2500.wav'

    stt_diarizer = DiarizedSTT()
    rs = stt_diarizer.diarize(test_fpath)

    print(rs)
