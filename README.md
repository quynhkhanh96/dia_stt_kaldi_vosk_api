## Setup 
- Install conda env with python 3.7, torch 1.4.0, numpy 
- Install pyannote-audio:
```
    git clone https://github.com/pyannote/pyannote-audio.git
    cd pyannote-audio
    git checkout develop
    pip install .
```
- Setup Kaldi:
    + Install Kaldi
    + Clone this project and `cd diarized_speech2text`
    + Change KALDI_ROOT in path.sh to the path to Kaldi directory and then `source path.sh` 
    + Create soft links:
    ```
    ln -s $KALDI_ROOT/egs/callhome_diarization/v1/diarization .
    ln -s $KALDI_ROOT/egs/callhome_diarization/v1/local .
    ln -s $KALDI_ROOT/egs/sre08/v1/sid .
    ln -s $KALDI_ROOT/egs/wsj/s5/steps .
    ln -s $KALDI_ROOT/egs/wsj/s5/utils . 
    ```
- Copy trained STT model from `slp/01_asr/02_asr_models/202005-STT-GeneralDomain_-3k5VinData_2kYoutube_XSAMPAPhones_ConventionalTraining_Bigmodel-Thinh-VoskAPI` and trained X-vector model from `/slp/exp` and put them into `diarized_speech2text` (copy the whole directory `202005-STT-GeneralDomain_-3k5VinData_2kYoutube_XSAMPAPhones_ConventionalTraining_Bigmodel-Thinh-VoskAPI` and `exp`)

## Usage 
    from diarized_speech2text import DiarizedSTT
    test_fpath = 'test.wav'
    stt_diarizer = DiarizedSTT()
    rs = stt_diarizer.diarize(test_fpath)

DiarizedSTT get path of an audio (.wav) file as input and return a json of the following format:

        diarized_transcripts = {
             'speaker_0': [
                 {
                     'start': 0.2,
                     'end': 3.5,
                     'text': 'Xin chào'
                 },
                 {
                     'start': 5.1,
                     'end': 7.3,
                     'text': 'Tên tôi là Nguyễn Văn A'
                 }
             ],
             'speaker_1': [
                 {
                     'start': 3.8,
                     'end': 4.5,
                     'text': 'chào A'
                 },
                 {
                     'start': 8.0,
                     'end': 9.4,
                     'text': 'Bạn sống ở đâu?'
                 }                
             ]
        }

For each speaker: 
- start: start time of each utterance
- end: end time of each utterance
- text: transcription of each utterance